<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\TaskController;
use Illuminate\Support\Facades\Route;


Route::get('test', function () {
    return 'welcome';
});

Route::controller(AuthController::class)->group(function () {
    Route::post('register', 'register');
    Route::post('login', 'login');
});

Route::middleware('auth:sanctum')->group(function () {
    Route::post('logout', [AuthController::class, 'logout']);

    // Route::controller(TaskController::class)->prefix('task')->group(function () {
        // Route::resource('/', TaskController::class)->except(['create', 'edit']);
    //     Route::post('/', 'store');
    //     Route::get('/', 'index');
    //     Route::get('/{id}', 'show');
    //     Route::put('/{id}',  'update');
    //     Route::delete('/{id}', 'destroy');
    //     Route::put('/{id}/completed', 'markAsCompleted');
    //     Route::put('/{id}/incomplete', 'markAsIncomplete');
    //     Route::put('/{id}/due-date','addDueDate');
    // });

    Route::prefix('task')->group(function () {
        Route::get('/', [TaskController::class, 'index']);
        Route::get('/sort', [TaskController::class, 'sortTasks']);
        Route::get('/filter', [TaskController::class,'filterTasks']);

        Route::post('/', [TaskController::class, 'store']);
        Route::post('/{taskId}/attachments', [TaskController::class, 'uploadAttachments']);
        Route::post('/{taskId}/tags', [TaskController::class, 'addTags']);
        Route::put('/{taskId}/archive', [TaskController::class, 'archiveTask']);
        Route::put('/{taskId}/restore', [TaskController::class, 'restoreTask']);

        Route::put('/{id}', [TaskController::class, 'update']);
        Route::put('/{id}/completed', [TaskController::class, 'markAsCompleted']);
        Route::put('/{id}/incomplete', [TaskController::class, 'markAsIncomplete']);
        Route::put('/{id}/due-date', [TaskController::class, 'addDueDate']);

        Route::delete('/{id}', [TaskController::class, 'destroy']);
    });
});
