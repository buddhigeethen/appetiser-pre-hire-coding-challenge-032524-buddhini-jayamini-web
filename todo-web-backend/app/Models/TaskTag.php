<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TagTask extends Model
{
    use HasFactory;

    protected $table = 'tag_task';

    protected $fillable = [
        'task_id',
        'tag_id'
    ];
}
