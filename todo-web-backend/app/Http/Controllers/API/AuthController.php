<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Throwable;

class AuthController extends BaseController
{
    public function login(Request $request)
    {
        try {
            $request->validate([
                'email' => 'required|email',
                'password' => 'required',
            ]);

            $credentials = $request->only('email', 'password');

            if (!Auth::attempt($credentials)) {
                return $this->sendError('Invalid credentials.', [], 422);
            }

            $user = Auth::user();
            $token = $user->createToken('authToken')->plainTextToken;

            $success['token'] =  $token;
            $success['user'] =  $user;

            return $this->sendResponse($success, 'User login successfully.');
        } catch (Throwable $e) {
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    public function register(Request $request)
    {
        try {
            $request->validate([
                'name' => 'required|string',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6',
            ]);

            DB::beginTransaction();

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            $token = $user->createToken('authToken')->plainTextToken;

            $success['token'] =  $token;
            $success['user'] =  $user;

            DB::commit();

            return $this->sendResponse($success, 'User registered successfully.');
        } catch (Throwable $e) {
             DB::rollBack();

            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    public function logout(Request $request)
    {
        try {
            $request->user()->tokens()->delete();
            return $this->sendResponse([], 'User logged out successfully.');
        } catch (Throwable $e) {
            return $this->sendError('Unable to logout.', ['error' => $e->getMessage()], 500);
        }
    }
}
