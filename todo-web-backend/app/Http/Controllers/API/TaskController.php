<?php

namespace App\Http\Controllers\API;

use App\Models\Task;
use App\Models\Attachment;
use App\Models\Tag;
use App\Repositories\TaskRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Throwable;

class TaskController extends BaseController
{

    protected $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tasks = $this->taskRepository->all();
        return $this->sendResponse($tasks, 'Tasks retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $data = $request->only(['title', 'description', 'due_date', 'priority']);
            $data['user_id'] = Auth::user()->id;
            $data['status'] = 'ToDo';

            $task = $this->taskRepository->create($data);

            return $this->sendResponse($task, 'Task created successfully.', 201);
        } catch (Throwable $e) {
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $task = $this->taskRepository->find($id);
        return $this->sendResponse($task, 'Task retrieved successfully.');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $id)
    {
        try {
            $data = $request->only(['title', 'description', 'due_date', 'priority']);
            $task = $this->taskRepository->update($id, $data);
            return $this->sendResponse($task, 'Task updated successfully.');
        } catch (Throwable $e) {
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        try {
            $this->taskRepository->delete($id);
            return $this->sendResponse([], 'Task deleted successfully.', 204);
        } catch (Throwable $e) {
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    public function markAsCompleted(Request $request, $id)
    {
        try {
            $user = Auth::user();
            $task = Task::findOrFail($id);

            // Check if the task belongs to the authenticated user
            if ($task->user_id !== $user->id) {
                return $this->sendError('Unauthorized.', [], 401);
            }

            // Mark the task as completed and set the date completed to now
            $task->completed_at = now()->format('Y-m-d H:m');
            $task->status = 'Complete';

            $task->save();

            return $this->sendResponse($task, 'Task marked as completed successfully.');
        } catch (Throwable $e) {
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    public function markAsIncomplete(Request $request, $id)
    {
        try {
            $user = Auth::user();
            $task = Task::findOrFail($id);

            // Check if the task belongs to the authenticated user
            if ($task->user_id !== $user->id) {
                return $this->sendError('Unauthorized.', [], 401);
            }

            // Mark the task as incomplete and set the date completed to null
            $task->completed_at = null;
            $task->status = 'InComplete';
            $task->save();

            return $this->sendResponse($task, 'Task marked as incomplete successfully.');
        } catch (Throwable $e) {
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    public function addDueDate(Request $request, $id)
    {
        try {
            $user = Auth::user();
            $task = Task::findOrFail($id);

            // Check if the task belongs to the authenticated user
            if ($task->user_id !== $user->id) {
                return $this->sendError('Unauthorized.', [], 401);
            }

            // Validate the due date format
            $request->validate([
                'due_date' => 'nullable|date_format:Y-m-d',
            ]);

            // Set the due date for the task
            $task->due_date = $request->due_date;
            $task->save();

            return $this->sendResponse($task, 'Due date added to task successfully.');
        } catch (Throwable $e) {
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    public function sortTasks(Request $request)
    {
        try {
            $user = Auth::user();
            $query = Task::where('user_id', $user->id);

            // Define default sort options
            $sortBy = $request->input('sort_by', 'created_at');
            $sortOrder = $request->input('sort_order', 'asc');

            // Validate sort by criteria
            $validSortBy = ['title', 'description', 'due_date', 'created_at', 'completed_at', 'priority'];
            if (!in_array($sortBy, $validSortBy)) {
                return $this->sendError('Error.', ['error' => 'Invalid sort by criteria.'], 422);
            }

            // Apply sorting
            $query->orderBy($sortBy, $sortOrder);

            // Paginate the sorted tasks
            $tasks = $query->paginate(10);

            return $this->sendResponse($tasks, 'sorted task successfully.');
        } catch (Throwable $e) {
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    public function filterTasks(Request $request)
    {
        try {
            $user = Auth::user();
            $query = Task::where('user_id', $user->id);

            // Filter by completed date range
            if ($request->has('completed_from')) {
                $query->whereDate('completed_at', '>=', $request->input('completed_from'));
            }
            if ($request->has('completed_to')) {
                $query->whereDate('completed_at', '<=', $request->input('completed_to'));
            }

            // Filter by priority
            if ($request->has('priority')) {
                $query->where('priority', $request->input('priority'));
            }

            // Filter by due date range
            if ($request->has('due_from')) {
                $query->whereDate('due_date', '>=', $request->input('due_from'));
            }
            if ($request->has('due_to')) {
                $query->whereDate('due_date', '<=', $request->input('due_to'));
            }

            // Filter by archived date range
            if ($request->has('archived_from')) {
                $query->whereDate('archived_at', '>=', $request->input('archived_from'));
            }
            if ($request->has('archived_to')) {
                $query->whereDate('archived_at', '<=', $request->input('archived_to'));
            }

            // Filter by search query
            if ($request->has('search')) {
                $searchTerm = $request->input('search');
                $query->where(function ($query) use ($searchTerm) {
                    $query->where('title', 'like', "%$searchTerm%")
                        ->orWhere('description', 'like', "%$searchTerm%");
                });
            }

            // Paginate the filtered tasks
            $tasks = $query->paginate(10);

            return $this->sendResponse($tasks, 'Tasks filtered successfully.');
        } catch (Throwable $e) {
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    public function uploadAttachments(Request $request, $taskId)
    {
        try {
            $user = Auth::user();

            // Check if the user owns the task
            $task = Task::where('id', $taskId)->where('user_id', $user->id)->first();
            if (!$task) {
                return $this->sendError('Task not found.', [], 404);
            }
            if ($task->user_id !== auth()->id()) {
                return $this->sendError('Unauthorized.', [], 401);
            }

            // Validate the uploaded files
            $validator = Validator::make($request->all(), [
                'attachments.*' => 'required|file|mimes:jpg,jpeg,png,svg,mp4,csv,txt,doc,docx|max:20480',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Invalid files.', ['errors' => $validator->errors()], 422);
            }

            // Handle file upload
            $attachments = [];
            foreach ($request->file('attachments') as $file) {
                $fileName = $file->getClientOriginalName();
                $filePath = $file->storeAs('attachments', $fileName); // Store file in the 'attachments' directory

                // Save attachment record to the database
                $attachment = new Attachment();
                $attachment->task_id = $task->id;
                $attachment->file_name = $fileName;
                $attachment->file_path = $filePath;
                $attachment->save();

                $attachments[] = $attachment;
            }

            return $this->sendResponse($attachments, 'Attachments uploaded successfully.');
        } catch (Throwable $e) {
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    public function addTags(Request $request, $taskId)
    {
        try {
            $user = Auth::user();

            // Check if the user owns the task
            $task = Task::findOrFail($taskId);
            if ($task->user_id !== auth()->id()) {
                return $this->sendError('Unauthorized.', [], 401);
            }

            // Validate the incoming tag data
            $validatedData = $request->validate([
                'tags' => 'required|array',
                'tags.*' => 'string|max:255',
            ]);

            // Process each tag and attach it to the task
            $tagIds = [];
            foreach ($validatedData['tags'] as $tagName) {
                $tag = Tag::firstOrCreate(['name' => $tagName]);
                $tagIds[] = $tag->id; // Extract the ID of the tag model
            }

            // Attach the tag IDs to the task
            $task->tags()->syncWithoutDetaching($tagIds);

            // Return the task with attached tags
            return $this->sendResponse($task->tags, 'Tags added successfully to the task.');
        } catch (Throwable $e) {
            // Handle any exceptions
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    public function archiveTask(Request $request, $taskId)
    {
        try {
            $user = $request->user();

            // Check if the user owns the task
            $task = Task::findOrFail($taskId);
            if ($task->user_id !== $user->id) {
                return $this->sendError('Unauthorized.', [], 401);
            }

            // Archive the task by setting the archived date to the current timestamp
            $task->archived_at = now()->format('Y-m-d H:m');
            $task->status = 'Archived';

            $task->save();

            return $this->sendResponse($task, 'Task Archived added successfully.');
        } catch (Throwable $e) {
            // Handle any exceptions
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }

    public function restoreTask(Request $request, $taskId)
    {
        try {
            $user = $request->user();

            // Check if the user owns the task
            $task = Task::findOrFail($taskId);
            if ($task->user_id !== $user->id) {
                return $this->sendError('Unauthorized.', [], 401);
            }

            // Restore the task by setting the archived date to NULL
            $task->archived_at = null;
            $task->status = 'Restore';

            $task->save();

            return $this->sendResponse($task, 'Task Restore added successfully.');
        } catch (Throwable $e) {
            // Handle any exceptions
            return $this->sendError('Error.', ['error' => $e->getMessage()]);
        }
    }
}
