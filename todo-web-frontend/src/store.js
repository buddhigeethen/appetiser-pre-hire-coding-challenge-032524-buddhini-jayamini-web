import { createStore } from 'vuex';
import { login, logout } from './login';
import { getTasks, createTask, updatedTask, deletedTask, archiveTask,restoreTask, markAsCompleted, markAsIncomplete  } from './task';

export default createStore({
  state: {
    user: null,
    token: null,
    tasks: [],
    isLoggedIn: false,
    loginError: '',
    taskError: ''
  },
  mutations: {
    setUser(state, user) {
      state.user = user;
    },
    setToken(state, token) {
      state.token = token;
    },
    setTasks(state, tasks) {
      state.tasks = tasks;
    },
    setIsLoggedIn(state, isLoggedIn) {
      state.isLoggedIn = isLoggedIn;
    },
    setLoginError(state, error) {
      state.loginError = error;
    },
    setTaskError(state, error) {
      state.taskError = error;
    }
  },
  actions: {
    async loginUser({ commit }, credentials) {
      try {
        const { user, token } = await login(credentials);
        commit('setUser', user);
        commit('setToken', token);
        commit('setIsLoggedIn', true);
        commit('setLoginError', '');
      } catch (error) {
        commit('setUser', null);
        commit('setToken', null);
        commit('setIsLoggedIn', false);
        commit('setLoginError', error.message);
      }
    },
    async logoutUser({ commit }) {
      await logout();
      commit('setUser', null);
      commit('setToken', null);
      commit('setIsLoggedIn', false);
    },
    async fetchTasks({ state, commit }) {
      try {
        const tasks = await getTasks(state.token);
        commit('setTasks', tasks);
        commit('setTaskError', '');
      } catch (error) {
        commit('setTasks', []);
        commit('setTaskError', error.message);
      }
    },
    async createNewTask({ state, dispatch, commit }, taskData) {
      try {
        await createTask(state.token, taskData);
        await dispatch('fetchTasks');
      } catch (error) {
        commit('setTaskError', error.message);
      }
    },
    async updateExistingTask({ state, dispatch, commit }, { taskId, taskData }) {
      try {
        await updatedTask(state.token, taskId, taskData);
        await dispatch('fetchTasks');
      } catch (error) {
        commit('setTaskError', error.message);
      }
    },
    async deleteExistingTask({ state, dispatch, commit }, taskId) {
      try {
        await deletedTask(state.token, taskId);
        await dispatch('fetchTasks');
      } catch (error) {
        commit('setTaskError', error.message);
      }
    }
  },
  modules: {}
});
