export async function login(email, password) {
    try {
      const response = await fetch('http://localhost:8000/api/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email, password })
      });
  
      if (!response.ok) {
        throw new Error('Login failed');
      }
  
      const data = await response.json();
      return data;
    } catch (error) {
      throw error;
    }
  }
  
  export const logout = async () => {
    try {
      const token = localStorage.getItem('token');
  
      const response = await fetch(LOGOUT_URL, {
        method: 'POST',
        headers: {
          'Authorization': `Bearer ${token}`,
        },
      });
  
      if (!response.ok) {
        throw new Error('Logout failed.');
      }
  
      // Remove the token from localStorage or Vuex state
      localStorage.removeItem('token');
    } catch (error) {
      throw new Error(error.message);
    }
  };