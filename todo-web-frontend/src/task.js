const BASE_URL = "http://localhost:8000/api/task";
let token = localStorage.getItem("token");

export const getTasks = async () => {
  try {
    const response = await fetch(BASE_URL, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (!response.ok) {
      throw new Error("Failed to fetch tasks");
    }
    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error(error.message);
  }
};

export const createTask = async (taskData) => {
  try {
    const response = await fetch(BASE_URL, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(taskData),
    });

    if (!response.ok) {
      throw new Error("Failed to create task");
    }
    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error(error.message);
  }
};

export const updatedTask = async (id, taskData) => {
  try {
    taskData['due_date'] = taskData['dueDate'];
    const response = await fetch(`${BASE_URL}/${id}`, {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify(taskData),
    });
    if (!response.ok) {
      throw new Error("Failed to update task");
    }
    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error(error.message);
  }
};

export const deletedTask = async (id) => {
  try {
    const response = await fetch(`${BASE_URL}/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    });
    if (!response.ok) {
      throw new Error("Failed to delete task");
    }
    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error(error.message);
  }
};
export const archiveTask = async (taskId) => {
  try {
    const response = await fetch(`${BASE_URL}/${taskId}/archive`,{
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    });
    window.location.reload();
    return response.data;
  } catch (error) {
    console.error("Error archiving task:", error);
    throw new Error("Failed to archive task");
  }
};

export const restoreTask = async (taskId) => {
  try {
    const response = await fetch(`${BASE_URL}/${taskId}/restore`,{
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    });
    window.location.reload();
    return response.data;
  } catch (error) {
    console.error("Error restoring task:", error);
    throw new Error("Failed to restore task");
  }
};

export const markAsCompleted = async (id) => {
  try {
    const response = await fetch(`${BASE_URL}/${id}/completed`,{
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    });
    window.location.reload();
    return response.data;
  } catch (error) {
    console.error("Error marking task as completed:", error);
    throw new Error("Failed to mark task as completed");
  }
};

export const markAsIncomplete = async (id) => {
  try {
    const response = await fetch(`${BASE_URL}/${id}/incomplete`,{
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    });
    window.location.reload();
    return response.data;
  } catch (error) {
    console.error("Error marking task as incomplete:", error);
    throw new Error("Failed to mark task as incomplete");
  }
};

export const sortTasks = async (sortBy, sortOrder) => {
  try {
    const response = await fetch(`${BASE_URL}/sort?sort_by=${sortBy}&sort_order=${sortOrder}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (!response.ok) {
      throw new Error("Failed to sort tasks");
    }
    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error(error.message);
  }
};

export const filterTasks = async (filterCriteria) => {
  try {
    const response = await fetch(`${BASE_URL}/filter?${filterCriteria}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    if (!response.ok) {
      throw new Error("Failed to filter tasks");
    }
    const data = await response.json();
    return data;
  } catch (error) {
    throw new Error(error.message);
  }
};