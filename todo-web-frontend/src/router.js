import { createRouter, createWebHistory } from 'vue-router';
import LoginPage from './pages/LoginPage.vue';
import TaskList from './pages/TaskList.vue'; 
import TaskCreate from './pages/TaskCreate.vue'; 

const routes = [
  { path: '/', redirect: '/login' },
  { path: '/login', component: LoginPage },
  {
    path: '/dashboard',
    name: 'TaskList',
    component: TaskList,
    meta: { requiresAuth: true } 
  },
  {
    path: '/tasks',
    name: 'TaskCreate',
    component: TaskCreate,
    meta: { requiresAuth: true } 
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Check if the user is authenticated
    const isAuthenticated = localStorage.getItem('token') !== null;
    
    if (isAuthenticated) {
      // User is authenticated, allow access to the requested route
      next();
    } else {
      // User is not authenticated, redirect to the login page
      next('/login');
    }
  } else {
    // No authentication required for this route, proceed as normal
    next();
  }
});

export default router;
